import random, os
from art import logo

def game():
    clear = lambda: os.system('cls' if os.name == 'nt' else 'clear')
    clear()

    print(logo+"\n")
    print("Welcome to the number guessing game")
    print("I am thinking a number between 1 and 100")

    EASY_LEVEL_CHOICES = 10
    HARD_LEVEL_CHOICES = 5

    game_over = False
    number = random.randint(1,100)
    level = input("Enter a level. Easy or Hard?: ").lower()

    if level == "easy":
        number_of_guesses = HARD_LEVEL_CHOICES
    elif level == "hard":
        number_of_guesses = EASY_LEVEL_CHOICES
    else:
        print("Invalid Input")
        game_over = True

    while not game_over:
        print(f"Remaining guesses: {number_of_guesses}")
        guess = int(input("Enter a number:- "))
        if guess == number:
            print(f"You have won!! The number was {number}")
            game_over = True
        elif guess > number:
            print("Too high")
            number_of_guesses -= 1
        elif guess < number:
            print("Too low")
            number_of_guesses -= 1
        if number_of_guesses == 0:
            game_over = True
            print("You lost!")
    
    if input("Play again? (Y/N): ").upper() == "Y":
        game()

game()
